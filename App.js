import React, {useState} from 'react';
import './App.css';

// example de composants utilisant un clone pour react

function App() {
  const [chien, setChien] = useState({nom: "medor"})
  const [numbers, setNumbers] = useState([0,5,6,8])
  function rename(){
    const nouveauChien = {...chien}
    nouveauChien.nom = "milou"
    setChien(nouveauChien)
  }
  function addNumber() {
    numbers.push(5)
    setNumbers([...numbers])
  }
  return <div>
    {chien.nom}
    <br/>
    {numbers}
    <button onClick={rename}>Renomer</button>
    <button onClick={addNumber}>add</button>
  </div>
}

export default App;
